import numpy as np
import networkx as nx


def _rho(D, target_fraction=0.02, mode='gaussian'):
    """Calculates the RL rho values from a distance matrix"""
    dcut = np.sort(D)[:,1 + int(len(D) * target_fraction)].mean()
    if mode == 'classic':
        r = np.array([len(np.where(d < dcut)[0]) for d in D])
    elif mode == 'gaussian':
        r = np.exp(-D ** 2/(dcut**2)).sum(axis=0)
    else:
        raise ValueError('Error: unknown density mode {}'.format(mode))
    return r

def _delta(D, rho):
    """Calculated the RL delta values for points in a distribution"""
    m = D.max() + 1.0
    dm = D + np.identity(len(D)) * m
    idel = [np.where(rho <= rho[i], m, dm[i]).argmin() for i in range(len(rho))]
    d = [D[i, idel[i]] for i in range(len(idel))]
    idel[rho.argmax()] = rho.argmax()
    d[rho.argmax()] = max(d) + 1.0
    return np.array(d), np.array(idel)

def _centres(d, sigma=10.0):
    """Find the centres in an RL clustering"""
    dmean = d.mean()
    dstdev = d.std()
    dthresh = dmean + dstdev * sigma
    centres = []
    for i in range(len(d)):
        if d[i] > dthresh:
            centres.append(i)
    return np.array(centres), dthresh

class RLClusterResult(object):
    """The result from an RL clustering"""
    def __init__(self):
        self.assignments = None
        self.rhos = None
        self.deltas = None
        self.centres = None
        self.threshold = None

def cluster(d, target_fraction=0.02, sigma=5.0, mode='gaussian', rho=None):
    """Do Rodriguez-Laio clustering on a square-form distance matrix"""
    D = np.array(d)
    if len(D.shape) != 2:
        raise ValueError('Error - the input is not a 2D matrix')
    if D.shape[0] != D.shape[1]:
        raise ValueError('Error - the input distance matrix is not square')
    if target_fraction < 0 or target_fraction > 1.0:
        raise ValueError('Error: target fraction must be between 0.0 and 1.0')
    if not mode in ['gaussian', 'classic']:
        raise ValueError('Error: unknown density mode {}'.format(mode))
    if rho is None:
        r = _rho(D, target_fraction=target_fraction, mode=mode)
    else:
        r = np.array(rho)
        if len(r.shape) != 1:
            raise ValueError('Error - rho must be a 1D vector')
        if len(r) != D.shape[0]:
            raise ValueError('Error - rho must be a vetor of length {}'.format(D.shape[0]))
    d, id = _delta(D, r)
    o, dthresh = _centres(d, sigma=sigma)
    dg = nx.DiGraph()
    for i in range(len(id)):
        if not i in o:
            dg.add_edge(i, id[i])
    wcc = sorted(nx.weakly_connected_components(dg), key=len, reverse=True)
    cids = -np.ones(len(d), dtype=int)
    id = 0
    for wc in wcc:
        cids[list(wc)] = id
        id += 1
    n_clusters = id
    result = RLClusterResult()
    result.assignments = cids
    result.centres = np.zeros(n_clusters, dtype=int)
    for i, j in enumerate(result.assignments[o]):
        if j > -1:
            result.centres[j] = o[i]
    result.rhos = np.array(r)
    result.deltas = np.array(d)
    result.threshold = dthresh
    return result

def decision_graph(result, axes):
    """Plot an RL decision graph on a set of matplotlib axes"""
    try:
        from matplotlib import pyplot as plt
    except ImportError:
        raise RuntimeError('Error: RLdecisionGraph() requires matplotlib')
    for id in np.unique(result.assignments):
        axes.plot(result.rhos[result.assignments==id], result.deltas[result.assignments==id], 'o')
    axes.plot([result.rhos.min(), result.rhos.max()], [result.threshold, result.threshold], '--')

